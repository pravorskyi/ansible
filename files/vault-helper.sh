#!/bin/sh
# The "kdewallet" is default wallet name
#FIXME: Create "ansible-vault-default" entry in KWalletManager UI, automatic creation via kwallet-query is not working
# https://bugs.kde.org/show_bug.cgi?id=491898

# Alternatives:
#     - keyring (via python3-keyring)
#     - kwalletcli

#FIXME: Supress warning "Qt: Session management error: None of the authentication protocols specified are supported"
# https://bugs.kde.org/show_bug.cgi?id=491903
unset SESSION_MANAGER

#FIXME: https://bugs.kde.org/show_bug.cgi?id=491938
export QT_QPA_PLATFORM=offscreen

kwallet-query kdewallet -r ansible-vault-default
