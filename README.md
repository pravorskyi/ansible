# README

`bootstrap-ansible-localhost.yaml` - a playbook designed to set up and configure Ansible on the control machine before running any other playbooks.

## Run examples

### First time run
`ansible-playbook bootstrap-ansible-localhost.yaml --become-method su -K -J`

### Regular run
`ansible-playbook bootstrap-ansible-localhost.yaml --become-method su -K`
`ansible-playbook hera.yaml`


## certbot
Run `certbot` manually to obtain TLS certificate and reconfigure Nginx
